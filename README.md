# Introduction

## Demo

[Open slider demo](https://www.devyaterikov.cloud/components/slider.html)
<br><br>

## Quick start

### CSS

#### Write the stylesheet `<link>` in your `<head>`:
`<link rel="stylesheet" href="https://www.devyaterikov.cloud/components/slider.css">`

### JS

#### Write the JS code `<script>` in your `<head>`:
`<script type="module" src="https://www.devyaterikov.cloud/components/slider.js"></script>`
<br><br>

## Download

[Download the archive with the project](https://www.devyaterikov.cloud/components/slider.zip), transfer the files to your project directory and connect them:

### CSS

#### Write the stylesheet `<link>` in your `<head>`:
`<link rel="stylesheet" href="./{path to slider.css}/slider.css">`

### JS

#### Write the JS code `<script>` in your `<head>`:
`<script type="module" src="./{path to slider.js}/slider.js"></script>`
<br><br><br>

# Documentation

## Including

#### Create an element with a class .slider:
`<div class="slider"></div>`

#### Set the slider settings:
`<div class="slider" slider-width="70" slider-ratio="0.4" automation-slider-time="8000"`<br>
`slider-transition-duration="1000" slider-transition-timing-function="ease"`<br>
`slider-content="[]"></div>`
<br><br>

## Attributes

#### Set the content to the slider:
`slider-content="[
    'red',
    'green',
    'blue',
    'http://link.com/image_path',
    './image_path'
]"`

#### Set the width to the slider (default value):
`slider-width="70"`

#### Set the ratio to the slider (default value):
`slider-ratio="0.4"`

#### Set the transition duration to the slider (default value):
`slider-transition-duration="1000"`

#### Set the transition timing function to the slider (default value):
`slider-transition-timing-function="ease"`

#### Set the automation switch time to the slider (default value):
`automation-slider-time="8000"`
