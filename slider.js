// Slider's consts
const default_width = 70
const default_ratio = 0.4
const default_slider_transition_duration = 1000
const default_slider_transition_timing_function = 'ease'
const default_automation_slider_time = 8000
const slider_widths = []
const slider_ratios = []
const slider_transition_durations = []
const slider_transition_timing_functions = []
const automation_slider_times = []
const slides_content = []

// Slider's global variables
let filled_dot_idxes = []
let slider_accesses = []
let slider_intervals = []
let slider_carts = []
let first_slides = []
let last_slides = []
let dots = []

// Sliders data
let sliders = []

// Function for getting slide content from HTML
function get_slider_content(slider_idx) {
    let content = sliders[slider_idx].getAttribute('slider-content')
    if (content) return JSON.parse(content.replaceAll("'", '"'))
    else return []
}

// Function for getting slider width from HTML
function get_slider_width(slider_idx) {
    let width = parseInt(sliders[slider_idx].getAttribute('slider-width'))
    if (width) return width
    else return default_width
}

// Function for getting slider ratio from HTML
function get_slider_ratio(slider_idx) {
    let ratio = parseInt(sliders[slider_idx].getAttribute('slider-ratio'))
    if (ratio) return ratio
    else return default_ratio

}

// Function for getting slider transition duration from HTML
function get_slider_transition_duration(slider_idx) {
    let transition_durations = parseInt(sliders[slider_idx].getAttribute('slider-transition-duration'))
    if (transition_durations) return transition_durations
    else return default_slider_transition_duration
}

// Function for getting slider transition timing function from HTML
function get_slider_transition_timing_function(slider_idx) {
    let transition_timing_function = sliders[slider_idx].getAttribute('slider-transition-function')
    if (transition_timing_function) return transition_timing_function
    else return default_slider_transition_timing_function
}

// Function for getting automation slider time from HTML
function get_automation_slider_time(slider_idx) {
    let automation_time = parseInt(sliders[slider_idx].getAttribute('automation-slider-time'))
    if (automation_time) return automation_time
    else return default_automation_slider_time
}

// Function for setting background in slide
function setting_background_in_slide(slide, content) {
    if (content.includes('http') || content.includes('images/'))
        slide.style.backgroundImage = `url(${content})`
    else slide.style.backgroundColor = content
}

// Function for check click in slider
function check_click_in_slider(event) {
    return !(event.target.classList.contains("dot") ||
    event.target.classList.contains("left_but") ||
    event.target.parentNode.classList.contains("left_but") ||
    event.target.parentNode.parentNode.classList.contains("left_but") ||
    event.target.classList.contains("right_but") ||
    event.target.parentNode.classList.contains("right_but") ||
    event.target.parentNode.parentNode.classList.contains("right_but"))
}


// Function for switching the slides (2 slides)
function slider_switch(slider_idx) {
    // Checking access
    if (slider_accesses[slider_idx]) {
        slider_accesses[slider_idx] = false
        setTimeout(() => {slider_accesses[slider_idx] = true}, 0.9 * slider_transition_durations[slider_idx])
        // Recreating interval for automation switching slides
        clearInterval(slider_intervals[slider_idx])
        slider_intervals[slider_idx] = setInterval(() => {slider_switch(slider_idx)}, automation_slider_times[slider_idx])
    }
    else return
    // Setting default transition-duration
    first_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    first_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    last_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    last_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    slider_carts[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    slider_carts[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    // Switching slides
    sliders[slider_idx].querySelector(".left_but").classList.toggle('disabled')
    sliders[slider_idx].querySelector(".right_but").classList.toggle('disabled')
    if (first_slides[slider_idx].style.left == "0vw") {
        first_slides[slider_idx].style.left = -slider_widths[slider_idx] + "vw"
        last_slides[slider_idx].style.left = "0vw"
    }
    else {
        last_slides[slider_idx].style.left = slider_widths[slider_idx] + "vw"
        first_slides[slider_idx].style.left = "0vw"
    }
    // Switching dots
    dots[slider_idx][filled_dot_idxes[slider_idx]].style.transitionDuration = 0.1 * slider_transition_durations[slider_idx] + "ms"
    dots[slider_idx][filled_dot_idxes[slider_idx]++].classList.toggle("fill")
    filled_dot_idxes[slider_idx] %= dots[slider_idx].length
    dots[slider_idx][filled_dot_idxes[slider_idx]].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    dots[slider_idx][filled_dot_idxes[slider_idx]].classList.toggle("fill")
}

// Function for switching right the slides (more 2 slides)
function slider_right(slider_idx) {
    // Checking access
    if (slider_accesses[slider_idx]) {
        slider_accesses[slider_idx] = false
        setTimeout(() => {slider_accesses[slider_idx] = true}, 0.9 * slider_transition_durations[slider_idx])
        // Recreating interval for automation switching slides
        clearInterval(slider_intervals[slider_idx])
        slider_intervals[slider_idx] = setInterval(() => {slider_right(slider_idx)}, automation_slider_times[slider_idx])
    }
    else return
    // Setting default transition-duration
    first_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    first_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    last_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    last_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    slider_carts[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    slider_carts[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    // Switching slides
    let cur_shift = Number(slider_carts[slider_idx].style.left.slice(0, -2))
    if (cur_shift == slider_widths[slider_idx]) {
        first_slides[slider_idx].style.left = -slider_widths[slider_idx] + "vw"
        last_slides[slider_idx].style.transitionDuration = "0ms"
        last_slides[slider_idx].style.left = slider_widths[slider_idx] + "vw"
        slider_carts[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        slider_carts[slider_idx].style.left = 0
    }
    else if (cur_shift == -(slides_content[slider_idx].length - 3) * slider_widths[slider_idx]) {
        first_slides[slider_idx].style.transitionDuration = "0ms"
        first_slides[slider_idx].style.left = slider_widths[slider_idx] + "vw"
        last_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        last_slides[slider_idx].style.left = 0
        slider_carts[slider_idx].style.left = cur_shift - slider_widths[slider_idx] + "vw"
    }
    else if (cur_shift == -(slides_content[slider_idx].length - 2) * slider_widths[slider_idx]) {
        first_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        first_slides[slider_idx].style.left = 0
        last_slides[slider_idx].style.left = -slider_widths[slider_idx] + "vw"
        slider_carts[slider_idx].style.transitionDuration = "0ms"
        slider_carts[slider_idx].style.left = slider_widths[slider_idx] + "vw"
    }
    else slider_carts[slider_idx].style.left = cur_shift - slider_widths[slider_idx] + "vw"
    // Switching dots
    dots[slider_idx][filled_dot_idxes[slider_idx]].style.transitionDuration = 0.1 * slider_transition_durations[slider_idx] + "ms"
    dots[slider_idx][filled_dot_idxes[slider_idx]++].classList.toggle("fill")
    filled_dot_idxes[slider_idx] %= dots[slider_idx].length
    dots[slider_idx][filled_dot_idxes[slider_idx]].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    dots[slider_idx][filled_dot_idxes[slider_idx]].classList.toggle("fill")
}

// Function for switching left the slides (more 2 slides)
function slider_left(slider_idx) {
    // Checking access
    if (slider_accesses[slider_idx]) {
        slider_accesses[slider_idx] = false
        setTimeout(() => {slider_accesses[slider_idx] = true}, 0.9 * slider_transition_durations[slider_idx])
        // Recreating interval for automation switching slides
        clearInterval(slider_intervals[slider_idx])
        slider_intervals[slider_idx] = setInterval(() => {slider_right(slider_idx)}, automation_slider_times[slider_idx])
    }
    else return
    // Setting default transition-duration
    first_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    first_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    last_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    last_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    slider_carts[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    slider_carts[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
    // Switching slides
    let cur_shift = Number(slider_carts[slider_idx].style.left.slice(0, -2))
    if (cur_shift == slider_widths[slider_idx]) {
        first_slides[slider_idx].style.left = slider_widths[slider_idx] + "vw"
        last_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        last_slides[slider_idx].style.left = 0
        slider_carts[slider_idx].style.transitionDuration = "0ms"
        slider_carts[slider_idx].style.left = -(slides_content[slider_idx].length - 2) * slider_widths[slider_idx] + "vw"
    }
    else if (cur_shift == -(slides_content[slider_idx].length - 2) * slider_widths[slider_idx]) {
        first_slides[slider_idx].style.transitionDuration = "0ms"
        first_slides[slider_idx].style.left = -slider_widths[slider_idx] + "vw"
        last_slides[slider_idx].style.left = slider_widths[slider_idx] + "vw"
        slider_carts[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        slider_carts[slider_idx].style.left = cur_shift + slider_widths[slider_idx] + "vw"
    }
    else if (cur_shift == 0) {
        first_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        first_slides[slider_idx].style.left = 0
        last_slides[slider_idx].style.transitionDuration = "0ms"
        last_slides[slider_idx].style.left = -slider_widths[slider_idx] + "vw"
        slider_carts[slider_idx].style.left = slider_widths[slider_idx] + "vw"
    }
    else slider_carts[slider_idx].style.left = cur_shift + slider_widths[slider_idx] + "vw"
    // Switching dots
    dots[slider_idx][filled_dot_idxes[slider_idx]].style.transitionDuration = 0.1 * slider_transition_durations[slider_idx] + "ms"
    dots[slider_idx][filled_dot_idxes[slider_idx]--].classList.toggle("fill")
    filled_dot_idxes[slider_idx] = (filled_dot_idxes[slider_idx] + dots[slider_idx].length) % dots[slider_idx].length
    dots[slider_idx][filled_dot_idxes[slider_idx]].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
    dots[slider_idx][filled_dot_idxes[slider_idx]].classList.toggle("fill")
}

// Function for switching slide to place
function slider_to_slide(slider_idx, slide_idx) {
    if (!slider_accesses[slider_idx]) return
    let cur_slide_idx = filled_dot_idxes[slider_idx]
    let cnt_switch = Math.abs(cur_slide_idx - slide_idx)
    let switch_function = undefined
    // Choose switch function
    if (slides_content[slider_idx].length == 2) switch_function = slider_switch
    else if (slide_idx > cur_slide_idx) switch_function = slider_right
    else if (slide_idx < cur_slide_idx) switch_function = slider_left
    else return
    // Go to slide
    slider_transition_durations[slider_idx] = 300
    slider_transition_timing_functions[slider_idx] = 'linear'
    switch_function(slider_idx)
    if (--cnt_switch <= 0) {
        slider_transition_durations[slider_idx] = 1000
        slider_transition_timing_functions[slider_idx] = 'ease'
        return
    }
    let slider_to_slide_interval = setInterval(() => {
        switch_function(slider_idx)
        if (--cnt_switch <= 0) {
            slider_transition_durations[slider_idx] = 1000
            slider_transition_timing_functions[slider_idx] = 'ease'
            clearInterval(slider_to_slide_interval)
        }
    }, slider_transition_durations[slider_idx])
}

window.onload = () => {
    sliders = document.querySelectorAll(".slider")
    for (let slider_idx = 0; slider_idx < sliders.length; ++slider_idx) {
        // Setting consts
        slider_widths.push(get_slider_width(slider_idx))
        slider_ratios.push(get_slider_ratio(slider_idx))
        slider_transition_durations.push(get_slider_transition_duration(slider_idx))
        slider_transition_timing_functions.push(get_slider_transition_timing_function(slider_idx))
        automation_slider_times.push(get_automation_slider_time(slider_idx))
        slides_content.push(get_slider_content(slider_idx))

        // Setting global variables
        filled_dot_idxes.push(0)
        slider_accesses.push(true)
        slider_intervals.push(undefined)
        slider_carts.push(undefined)
        first_slides.push(undefined)
        last_slides.push(undefined)
        dots.push([])

        if (slides_content[slider_idx].length == 0) {
            sliders[slider_idx].style.display = "none"
            continue
        }

        // Adding default template in DOM
        sliders[slider_idx].innerHTML =
        `<div class="viewer"><div class="slider_cart"></div></div>
        <button class="left_but" style="
            width: ${0.04 * slider_widths[slider_idx] + "vw"};
            transition: ${0.2 * slider_transition_durations[slider_idx]}ms;
            top: ${0.4 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"};
        ">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-left" class="svg-inline--fa fa-angle-left fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z"></path></svg>
        </button>
        <button class="right_but" style="
            width: ${0.04 * slider_widths[slider_idx] + "vw"};
            transition: ${0.2 * slider_transition_durations[slider_idx]}ms;
            top: ${0.4 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"};
        ">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" class="svg-inline--fa fa-angle-right fa-w-8" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path></svg>
        </button>
        <div class="dots_nav"></div>`

        // Assigning default slider style
        sliders[slider_idx].style.width = slider_widths[slider_idx] + "vw"
        sliders[slider_idx].style.height = slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"
        sliders[slider_idx].style.marginBottom = 1.5 * 0.11 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"

        //  Assigning default switch buttons style
        slider_carts[slider_idx] = sliders[slider_idx].querySelector(".viewer > .slider_cart")
        if (slides_content[slider_idx].length == 1) {
            sliders[slider_idx].querySelector(".left_but").style.display = "none"
            sliders[slider_idx].querySelector(".right_but").style.display = "none"
        }
        else if (slides_content[slider_idx].length == 2)
            sliders[slider_idx].querySelector(".left_but").classList.add('disabled')

        // Creating last slide
        last_slides[slider_idx] = document.createElement('div')
        last_slides[slider_idx].classList.add('last_slide')
        last_slides[slider_idx].style.width = slider_widths[slider_idx] + "vw"
        slider_carts[slider_idx].after(last_slides[slider_idx])
        if (slides_content[slider_idx].length == 2)
            last_slides[slider_idx].style.left = slider_widths[slider_idx] + "vw"
        else last_slides[slider_idx].style.left = -slider_widths[slider_idx] + "vw"
        last_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        last_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
        setting_background_in_slide(last_slides[slider_idx], slides_content[slider_idx][slides_content[slider_idx].length - 1])

        // Creating first slide
        first_slides[slider_idx] = document.createElement('div')
        first_slides[slider_idx].classList.add('first_slide')
        first_slides[slider_idx].style.width = slider_widths[slider_idx] + "vw"
        slider_carts[slider_idx].after(first_slides[slider_idx])
        first_slides[slider_idx].style.left = "0vw"
        first_slides[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
        first_slides[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]
        setting_background_in_slide(first_slides[slider_idx], slides_content[slider_idx][0])

        if (slides_content[slider_idx].length > 1) {
            // Assigning default slider_cart style
            slider_carts[slider_idx].style.width = (slides_content[slider_idx].length - 2) * 100 + "%"
            slider_carts[slider_idx].style.left = slider_widths[slider_idx] + "vw"
            slider_carts[slider_idx].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"
            slider_carts[slider_idx].style.transitionTimingFunction = slider_transition_timing_functions[slider_idx]

            // Creating slides
            for (let slide_idx = 1; slide_idx < slides_content[slider_idx].length - 1; ++slide_idx) {
                let slide = document.createElement('div')
                slide.classList.add('elem')
                slide.style.width = slider_widths[slider_idx] + "vw"
                slider_carts[slider_idx].append(slide)
                setting_background_in_slide(slide, slides_content[slider_idx][slide_idx])
            }

            // Creating navigation dots
            let dots_nav = sliders[slider_idx].querySelector(".slider .dots_nav")
            dots_nav.style.bottom = -0.11 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"
            for (let dot_idx = 0; dot_idx < slides_content[slider_idx].length; ++dot_idx) {
                let dot = document.createElement('div')
                dot.classList.add('dot')
                dot.style.width = 0.04 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"
                dot.style.height = 0.04 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"
                dot.style.margin = `${0.02 * slider_ratios[slider_idx] * slider_widths[slider_idx] + "vw"} ${0.01 * slider_widths[slider_idx] + "vw"}`
                dot.style.transitionDuration = 0.1 * slider_transition_durations[slider_idx] + "ms"
                dots_nav.append(dot)
            }
            dots[slider_idx] = dots_nav.querySelectorAll('.dot')
            dots[slider_idx][0].classList.add('fill')
            dots[slider_idx][0].style.transitionDuration = slider_transition_durations[slider_idx] + "ms"

            // Event listener for dots
            for (let dot_idx = 0; dot_idx < dots[slider_idx].length; ++dot_idx) {
                dots[slider_idx][dot_idx].onclick = () => {
                    slider_to_slide(slider_idx, dot_idx)
                }
            }

            // Slider's event listeneres
            sliders[slider_idx].onclick = (event) => {
                if (!check_click_in_slider(event)) return
                console.log('Click on slider')
            }

            // Setting slider's switch functions
            const slider_switch_left_function = (slides_content[slider_idx].length == 2) ? slider_switch : slider_left
            const slider_switch_right_function = (slides_content[slider_idx].length == 2) ? slider_switch : slider_right

            // Creating interval for automation switch slides
            slider_intervals[slider_idx] = setInterval(() => {slider_switch_right_function(slider_idx)}, automation_slider_times[slider_idx])

            // Event listenters for slider's buttons
            sliders[slider_idx].querySelector(".left_but").onclick = (event) => {
                if (sliders[slider_idx].querySelector(".left_but").classList.contains("disabled"))
                    return
                slider_switch_left_function(slider_idx)
            }
            sliders[slider_idx].querySelector(".right_but").onclick = (event) => {
                if (sliders[slider_idx].querySelector(".right_but").classList.contains("disabled"))
                    return
                slider_switch_right_function(slider_idx)
            }
        }
    }
}